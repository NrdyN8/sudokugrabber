<?php
header('Content-Type: application/json');
class SudokuGrabber{
    public static function getJSON($solved = false){
        $sudokuUrl = file_get_contents('http://www.sudokuweb.org');
        $sudokuArray = array("lines" => []);

        $dom = new domDocument;
        $dom->loadHTML($sudokuUrl);
        $sudoku = $dom->getElementById('sudoku');
        foreach ($sudoku->getElementsByTagName("tr") as $tr){
            $lineArray =[];
            foreach($tr->getElementsByTagName("td") as $td){
                if($td->lastChild->nodeName == "span"){
                    array_push($lineArray, 0);
                }
                else{
                    array_push($lineArray,intval($td->textContent));
                }
            }
            array_push($sudokuArray["lines"], $lineArray);
        }
        return json_encode($sudokuArray);
    }
}
echo(SudokuGrabber::getJSON());
